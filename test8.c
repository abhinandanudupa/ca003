#include <stdio.h>

int main()
{
    typedef struct employee{
        int empid;
        char name[20];
        double sal;
        char design[20];
        float exper;
    }employee;

    employee emp;
    printf("Enter the name of the employee :");
    scanf("\r%s",emp.name);
    printf("Enter the Employee ID :");
    scanf("\r%i", &emp.empid);
    printf("Enter the designation of the employee :");
    scanf("\r%s",emp.design);
    printf("Enter the salary earned by the employee :");
    scanf("\r%lf", &emp.sal);
    printf("What is the experience level of the employee in years :");
    scanf("\r%f", &emp.exper);


    printf("\nName :%s\nEmployee ID :%i\nDesignation :%s\nSalary :%.2lf\nExperience level :%.1f\n", emp.name, emp.empid, emp.design, emp.sal, emp.exper);
    return 0;
}
