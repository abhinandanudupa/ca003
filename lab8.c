
#include <stdio.h>
#include <string.h>

int chk_palin(int len, char c_s[])
{
    int c=0, check=0;
    for(; c<len/2; c++)
    {
        if(c_s[c]==c_s[len-1-c])
        {
            check+=1;
        }
    }
    if(check==len/2)
        return 1;
    else
        return 0;
}


void input(char str_strm[])
{
    printf("Enter the string you want to check :");
    gets(str_strm);
}




int main()
{
    char s[100];
    input(s);
    if(chk_palin(strlen(s), s))
    {
        printf("Your string [%s] is a palindrome.", s);
    }
    else
    {
        printf("Your string [%s] is NOT a palindrome.", s);
    }

    return 0;
}

