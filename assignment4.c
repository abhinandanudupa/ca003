#include <stdio.h>
#include <math.h>
#define pi 3.14285714286
int main()
{
    printf("\n\nProgram to calculate the Volume of a Sphere or Cylinder\nEnter 'S' to calculate the volume of a sphere \nOr Enter 'C' to calculate the volume of a cylinder\nYour choice:? ");
    char choice;
    scanf("%c", &choice);
    if (choice=='S' || choice=='s')
    {		printf("You have chosen to find the volume of a sphere\n");
     printf("Enter the radius of the sphere: ");
     float radius1;
     scanf("%f", &radius1);
     float volume=4*pi*pow(radius1, 3)/3;
     printf("The voulme of the Sphere is: %.4f\n", volume);
    }
    else if(choice=='C' || choice=='c')
    {
        printf("You have chosen to find the volume of a cylinder\n");
        printf("Enter the height of the cylinder: ");
        float height;
        scanf("%f", &height);
        printf("Enter the radius of base of the cylinder: ");
        float radius2;
        scanf("%f", &radius2);
        float volume=pow(radius2,2)*height*pi;
        printf("The volume of your cylinder is : %.4f\n", volume);

    }
    else

        printf("You have entered the wrong choice!!!\n \t\t\tTry again......");

}
