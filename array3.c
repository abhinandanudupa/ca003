#include <stdio.h>
#include <math.h>
int main()
{
    printf("Enter the number of values you for which you want to find the squares and cubes: ");
    int len;
    scanf("%i", &len);
    if(len>0)
    {
        float list[len], squares[len], cubes[len];
        int c;
        for(c=0; c<len; c++)
        {
            printf("Enter no.%i :", c+1);
            float num;
            scanf("\r%f", &num);
            list[c]=num;
        }
        for(c=0; c<len; c++)
        {
            squares[c]=pow(list[c], 2);
        }
        for(c=0; c<len; c++)
        {
            cubes[c]=pow(list[c], 3);
        }

        printf("\nThe squares are:\n");

        for(c=0; c<len; c++)
        {
            printf("%f ** 2 = %.4f\n", list[c], squares[c]);
        }

        printf("\nThe cubes are:\n");
        for(c=0; c<len; c++)
        {
            printf("%f ** 3 = %.4f\n", list[c], cubes[c]);
        }
    }
    else
    {
        printf("Enter a number greater than 0.Try again.....\n");
        main();
    }



    return 0;
}
