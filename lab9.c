#include <stdio.h>
#include <stdlib.h>

int main()
{
    typedef struct {
        int rollno;
        char name[20];
        char section;
        char department[10];
        float fee;
        float total_marks;
    }student;
    student stud1, stud2;



    printf("Enter the roll no. of the first student :");
    scanf("\r%i",&stud1.rollno);
    printf("Enter the name of the first student :");
    scanf("\r%19s", stud1.name);
    printf("Enter the section of the first student :");
    scanf("\r%c",&stud1.section);
    printf("Enter the department of the first student :");
    scanf("\r%19s", stud1.department);
    printf("Enter the fee amount if the first student :");
    scanf("\r%f",&stud1.fee);
    printf("Enter the total marks obtained by the student :");
    scanf("\r%f",&stud1.total_marks);


    printf("Enter the roll no. of the second student :");
    scanf("\r%i",&stud2.rollno);
    printf("Enter the name of the second student :");
    scanf("%19s", stud2.name);
    printf("Enter the section of the second student :");
    scanf("\r%c",&stud2.section);
    printf("Enter the department of the second student :");
    scanf("%19s", stud2.department);
    printf("Enter the fee amount if the second student :");
    scanf("\r%f",&stud2.fee);
    printf("Enter the total marks obtained by the student :");
    scanf("\r%f",&stud2.total_marks);

    if(stud2.total_marks>stud1.total_marks)
    {
        printf("The student with the highest marks is:\nRoll no.-%i\nName-%s\nSection-%c\nDepartment-%s\nFee amount-%f\nTotal Marks-%f\n", stud2.rollno, stud2.name, stud2.section, stud2.department, stud2.fee, stud2.total_marks);
    }
    else if(stud1.total_marks>stud2.total_marks)
    {
        printf("The student with the highest marks is:\nRoll no.-%i\nName-%s\nSection-%c\nDepartment-%s\nFee amount-%f\nTotal Marks-%f\n", stud1.rollno, stud1.name, stud1.section, stud1.department, stud1.fee, stud1.total_marks);
    }
    else
    {
        printf("Both students have got equal marks!\n");
        printf("The student with the highest marks is:\nRoll no.-%i\nName-%s\nSection-%c\nDepartment-%s\nFee amount-%f\nTotal Marks-%f\n", stud1.rollno, stud1.name, stud1.section, stud1.department, stud1.fee, stud1.total_marks);
        printf("The student with the highest marks is:\nRoll no.-%i\nName-%s\nSection-%c\nDepartment-%s\nFee amount-%f\nTotal Marks-%f\n", stud2.rollno, stud2.name, stud2.section, stud2.department, stud2.fee, stud2.total_marks);
    }

    return 0;
}
