#include <stdio.h>
int toOdd(int num)
{
	if (num>1)
	{
		return ++num;
	}
	else
	return num++;
}
int toEven(int num)
{
	return ++num;
}
int check(int num)
{
	if (num%2==0)
	{
		return 0;
	}
	else 
	{
		return 1;
	}
}

int main()
{
	printf("Enter any number greater than zero : ");
	int number;
	scanf("%i", &number);
	if (number>0)
	{
		if (check(number)==0)
		{
			printf("Your number is a even number.\n");
			printf("Converting your number to a odd number, we get : %i\n\n", toOdd(number));
		}
		else
		{
			printf("Your number is a odd number.\n");
			printf("Converting your number to a even number, we get : %i\n\n", toEven(number));
		}
	}
	else
	{
		printf("Invalid input!\nPlease Try again....\n");
	}
	return 0;
}
