
#include <stdio.h>

int main()
{
    int stu_mak[5][3];
    int i, j;
    for(j=0; j<5; j++)
    {
        for(i=0; i<3; i++)
        {
            printf("Enter the marks obtained by STUDENT %i in Subject %i: ", j+1, i+1);
            int mak;
            scanf("\r%i", &mak);
            if(mak<=100&&mak>-1)
            stu_mak[j][i]=mak;
            else
            {
                printf("\nPlease enter the marks in the range 0-100!!\n\n");
                i--;
            }
        }
        printf("\n");
    }
    int ms1=0, ms2=0, ms3=0;
    for(j=0; j<5; j++)
    {
        if(stu_mak[j][0]>stu_mak[ms1][0])
        {
            ms1=j;
        }
        if(stu_mak[j][1]>stu_mak[ms2][1])
        {
            ms2=j;
        }
        if(stu_mak[j][2]>stu_mak[ms3][2])
        {
            ms3=j;
        }
    }

    printf("The highest markes in subject 1 is %i\nThe highest markes in subject 2 is %i\nThe highest markes in subject 3 is %i\n", stu_mak[ms1][0], stu_mak[ms2][1], stu_mak[ms3][2]);

    return 0;
}

