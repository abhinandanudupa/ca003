#include <stdio.h>

int main()
{
    printf("Enter the name(max 19 characters) of the file in which you want to store the product details :");
    char f_n[20];
    scanf("\r%19s", f_n);
    fflush(stdin);
    FILE *s_f, *temp;//creating a temporary file 'temp' to store initial data
    temp=fopen("temp", "w+");
    s_f = fopen(f_n,"w+");

    char p_n[30];
    float price;
    printf("\nPlease enter at least three product details!!!\n");
    char key='2';
    while(1)
    {

        printf("Enter the name(max 29 characters) of the product :");
        fscanf(stdin, "%s", p_n);
        fflush(stdin);
        printf("Enter the price of the product :");
        fscanf(stdin, "\r%f", &price);
        printf("Enter 'x' to exit or enter any other character to continue adding another product detail:");
        fscanf(stdin, "\r%c", &key);
        fflush(stdin);
        fprintf(temp, "%s %.2f\n", p_n, price);
        if(key=='x'||key=='X')
            break;
    }

    fclose(temp);


    printf("Do you want to update the price (increase by 10 percentage) of the products?\n\nIf yes enter 'y' else any other chracter :");
    char up;
    scanf("\r%c", &up);
    fflush(stdin);

    if(up=='y'||up=='Y')
    {
        temp=fopen("temp", "r");
        printf("\nNow updating the price in your file...\n\nThe updated data is:\n");
        while(fscanf(temp, "%s %f",p_n, &price)!=EOF)
        {
            price*=1.1;
            fprintf(s_f, "%s %.2f\n",p_n, price);
        }

        fclose(temp);
        fclose(s_f);


        remove("temp");//deleting the temp file


        s_f=fopen(f_n, "r");
        while(fscanf(s_f, "%s %f\n",p_n, &price)!=EOF)
        {
            printf("%s  %f\n", p_n, price);
        }
        fclose(s_f);
    }
    else
    {
        printf("Your data is safely stored in the file '%s' \n\t\t\t\t:)\n", f_n);
        fclose(s_f);
        rename("temp", f_n);
    }


    return 0;
}
