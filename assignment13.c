#include <stdio.h>

int min(int *p, int *q)
{
    if(*p>*q)
        return *q;
    else if(*q>*p)
        return *p;
    else
        return -1;//both are equal
}

int gcd(int *p, int *q, int *g)
{
    if(min(p, q)!=-1)
    {
        int c=1;
        for(; c<=min(p, q); c++)
        {
            if(*p%c==0&&*q%c==0)
                *g=c;
        }
    }
    else
        *g=*p;

    return 0;
}

int input(int *p,int *q)
{
    while(*p<1)
    {
        printf("Enter the first number(it should be more than zero or else you will be prompted again) :\n");
        scanf("\r%i", p);
    }
    while(*q<1)
    {

        printf("Enter the second number(it should be more than zero or else you will be prompted again) :\n");
        scanf("\r%i", q);
    }
    return 0;
}

int display(int *g)
{
    if(*g!=0)
        printf("The GCD of the given numbers is :%i\n", *g);
    return 0;
}
int main()
{
    int n1=0, n2=0, GCD=0;
    input(&n1,&n2);
    gcd(&n1, &n2, &GCD);
    display(&GCD);
    return 0;
}
