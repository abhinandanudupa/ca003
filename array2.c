#include <stdio.h>
int main()
{
    printf("Enter the number of integers you are going to enter: ");
    int len;
    scanf("%i", &len);
    int c;

    if (len>0)
    {
        int list[len], order[len];
        for(c=0; c<len; c++)
        {
            printf("Enter integer no.%i :", c+1);
            scanf("\r%i",&list[c] );
            order[c]=list[c];
        }
            //sorting the array in descending order....
        for(c=0; c<(sizeof(order)/sizeof(order[0])); c++)
        {
            int count;
            for(count=0; count<len-1; count++)
            {
                if(order[count]<order[count+1])
                {
                    int d = order[count];
                    order[count]=order[count+1];
                    order[count+1]=d;
                }
            }
        }
        int index;
        for(c=0; c<len; c++)
        {
            if(order[1]==list[c])
            {
                index=c;
                break;
            }
        }

        printf("The 2nd largest integer is %i and is located at index position %i\n", order[1], index);
        return 0;
    }
    else
    {
        printf("\nEnter a number more than 0\nTry Again.......\n");
        return main();
    }

}
