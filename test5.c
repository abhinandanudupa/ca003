#include <stdio.h>

int main()
{
    printf("Enter the number of elements you are going to enter: ");
    int len;
    scanf("%i", &len);
    int c;
    int sm_index=0, la_index=0;
    if (len>0)
    {
        int list[len];
        for(c=0; c<len; c++)
        {
            printf("Enter no.%i :", c+1);
            scanf("\r%i",&list[c] );
            if(list[c]>list[la_index])
                la_index=c;
            if(list[c]<list[sm_index])
                sm_index=c;
        }
        printf("Now interchanging the smallest and the largest intergers...\nThe new array is:\n");
        c=list[sm_index];
        list[sm_index]=list[la_index];
        list[la_index]=c;
        for(c=0; c<len; c++)
        {
            printf("%5i   %i\n", c, list[c]);
        }

    }
    else
    {
        printf("Enter a number greater than 0.\nTry again.....\n");
        return main();
    }

    return 0;
}

