#include <stdio.h>

int main()
{
    int len;
    printf("Enter the no. of values you will enter: ");
    scanf("%i", &len);
    if(len>0)
    {

        float list[len];
        int c;
        for(c=0; c<len; c++)
        {
            printf("Enter no.%i : ", c+1);
            float d;
            scanf("\r%f", &d);
            list[c]=d;
        }
        //sorting array in descending order
        for(c=0; c<len; c++)
        {
            int f;
            for(f=0; f<len-1; f++)
            {
                if(list[f]<list[f+1])
                {
                    float g=list[f];
                    list[f]=list[f+1];
                    list[f+1]=g;
                }
            }
        }

        int duplicates=0, last_index=0;
        for(c=0; c<len;c++)
        {
            int f;
            for(f=len-1; f>last_index; f--)
            {
                if(list[f]==list[c])
                {
                    duplicates+=f-c;
                    last_index=f;
                    break;
                }
            }

        }
        printf("\nThe no. of duplicate (excess 1st, 2nd .... copies so on) elements in your list is %i\n", duplicates);
    }
    else
    {
        printf("Enter a value greater than 0. Try again.....\n");
        main();
    }


    return 0;
}
