#include <stdio.h>

int main()
{
    printf("Enter the integer for which you want to find the multiples (1-100): ");
    int num;
    scanf("%i", &num);
    int c;
    if (num>0)
    {
        int list[100/num];
        for(c=0; c<100/num; c++)
        {
            list[c]=num*(c+1);
        }
        printf("The multiples of %i are: \n", num);
        for(c=0; c<100/num; c++)
        {
            printf("Multiple no.%3i is %i\n", c+1, list[c]);
        }

    }
    else
    {
        printf("\nEnter a integer between 1  and 100.\nTry again...\n");
        main();
    }
    return 0;
}
