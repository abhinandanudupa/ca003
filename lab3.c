#include <stdio.h>
#include <math.h>
float root1(float a, float b, float c)
{
	return (-b+pow(b*b-4*c*a,0.5))/(2.0*a);
}
float root2(float a, float b, float c)
{
	return (-b-pow(b*b-4*c*a,0.5))/(2.0*a);
}
int main()
{
	printf("Enter the coefficient of the square of 'x' : ");
	float a, b ,c;
	scanf("%f", &a);
	if (a==0)
	{
		printf("The coeficient here cannot be zero!!\nTry again!!");
		main();
	}
	else
	{
		
		printf("Enter the coefficient of the 'x' term : ");
		scanf("%f", &b);
		printf("Enter the constant term : ");
		scanf("%f", &c);
		float r1=root1(a, b, c), r2 = root2(a, b, c);
		float determinant=b*b-4*a*c;
		if(determinant>0)
		{
			printf("The roots are real and distinct.\nThe roots of your quadratic equation are: %f and %f \n", r1, r2);
		}
		else if(determinant==0)
		{
			printf("The roots are real and equal.\nThe roots of your quadratic equation are: %f \n", r1);
		}
		else
		{
			float real_part=-b/(2.0*a), imag1=-pow(-determinant, 0.5)/(2*a), imag2=+pow(-determinant, 0.5)/(2*a);
		
			printf("The roots of your quadratic equation  are imaginary.\nAnd the roots are: %f + %fj and %f %fj\n", real_part, imag1, real_part, imag2);
		}
	}

	return 0;
}

