#include <stdio.h>
#include <stdlib.h>

typedef struct complex{
    double re;
    double im;
}complex;



void input(complex *no)
{
    printf("Enter your complex number in the format 'x+yi' ,i.e with any spaces between the number and symbols :");
    scanf("\r%lf %lfi", &(no->re), &(no->im));
    printf("Your number :%lf   %lfi\n",no->re, no->im);
}

void add(complex *no1, complex *no2, complex *r)
{
    r->re=(no1->re) + (no2->re);
    r->im=(no1->im) + (no2->im);
}
void sub(complex *no1, complex *no2, complex *r)
{
    r->re=(no1->re) - (no2->re);
    r->im=(no1->im) - (no2->im);
}
void display(complex *s)
{
    printf("The result of your operation is :%.4lf + %.4lfi", s->re, s->im);
}

int main()
{
    complex *num1=malloc(sizeof(complex));
    complex *num2=malloc(sizeof(complex));
    complex *result=malloc(sizeof(complex));
    printf("\n1st Complex number\n");
    input(num1);
    printf("\n2nd Complex number\n");
    input(num2);
    printf("Do you want to add or substract your numbers (enter 'a' for addition or 's' for substraction)?\n");
    char ch='k';
    while(ch!='s'||ch!='a')
    {

        scanf("\r%c", &ch);

        if(ch=='a')
        {
            add(num1, num2, result);
            break;
        }
        else if(ch=='s')
        {
            sub(num1, num2, result);
            break;
        }
        else
        {
            printf("Invalid choice!\nEnter the your choice again :");
        }
    }

    display(result);
    free(num1);
    free(num2);
    free(result);

    return 0;
}
