#include <stdio.h>
float InCoor(char ch)
{
	float c;
	printf("Enter the '%c' coordinate of your point: ", ch);
	scanf("%f", &c);
	return c;
}
int quadrant(float x_c, float y_c)
{
	if(x_c >0 && y_c>0)
	return 1;
	else if(x_c<0 && y_c>0)
	return 2;
	else if(x_c<0 && y_c<0)
	return 3;
	else if(x_c>0 && y_c<0) 
	return 4;
	else if(x_c==0 && y_c==0)
	return 5;
	else if(x_c==0 && y_c>0)
	{
		return 6;
	}
	else if(x_c==0 && y_c<0)
	{
		return 7;
	}
	else if(y_c==0 && x_c>0)
	{
		return 8;
	}
	else if(y_c==0 && x_c<0)
	{
		return 9;
	}
}
void display(int q)
{
	if (q==1)
	{
		printf("Your point is in the first quadrant\n");
	}
	else if (q==2)
	{
		printf("Your point is in the second quadrant\n");
	}
	else if(q==3)
	{
		printf("Your point is in the third quadrant\n");
	}
	else if(q==4)
	{
		printf("Your point is in the fourth quadrant\n");
	}
	else if(q==5)
	{
		printf("Your point is the Origin\n");
	}
	else if(q==6)
	{
		 printf("Your point is in the first or second quadrant\n");
	 }
	 else if(q==7)
	 {
		 printf("Your point is in the third or fourth quadrant\n");
	 }
	 else if(q==8)
	 {
		 printf("Your point is in the first or fourth quadrant\n");
	 }
	 else if(q==9)
	 {
		 printf("Your point is in the second or third quadrant\n");
	 }
	 
}
int main()
{
	float x, y;
	x=InCoor('x');
	y=InCoor('y');
	display(quadrant(x,y));
	return 0;
}
