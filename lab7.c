

#include <stdio.h>


int transpose(int k, int mat[k][k])
{
    int i=0, j=0;
    for(; i<k-1; i++)
    {
        for(j=i+1; j<k; j++)
        {

                int val=mat[i][j];
                mat[i][j]=mat[j][i];
                mat[j][i]=val;

        }
    }
    return 0;
}

int display(int k, int mat[k][k])
{
    printf("\n");
    int i=0, j=0;
    for(; i<k; i++)
    {
        for(j=0; j<k; j++)
        {
            printf("%4i\t", mat[i][j]);
        }
        printf("\n");
    }
    return 0;
}

int initialize(int k, int mat[k][k])
{
    int i=0, j=0;
    for(; i<k; i++)
    {
        for(j=0; j<k; j++)
        {
            mat[i][j]=0;
        }
    }
    return 0;
}


int input(int k, int mat[k][k])
{
    initialize(k, mat);
    int i=0, j=0;
    for(; i<k; i++)
    {
        for(j=0; j<k; j++)
        {
            printf("Enter element of row no. %i and column no.%i :", i+1, j+1);
            scanf("\r%i", &mat[i][j]);
            printf("Your matrix is now :\n");
            display(k, mat);
        }
    }
    return 0;
}



int main()
{
    printf("Enter the no. of rows(which is the same as no. of columns) of your square matrix :");
    int rc;
    scanf("%i", &rc);
    int Matrix[rc][rc];
    input(rc, Matrix);
    printf("\nThe transpose of your matrix is :\n");
    transpose(rc, Matrix);
    display(rc, Matrix);



    return 0;
}

