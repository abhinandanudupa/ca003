#include <stdio.h>
int main()
{
    printf("Enter the principal amount: ");
    float principal;
    scanf("%f", &principal);
    printf("Enter the percentage rate of interest: ");
    float rate;
    scanf("%f", &rate);
    printf("Enter the time period in years: ");
    float time;
    scanf("%f", &time);
    float interest=principal*rate*time/100;
    printf("The interest generated for the time %.1f years is : %.2f \n",time, interest );
    return 0;
}
