#include <stdio.h>

int main()
{
    printf("Enter the first number which will be stored in 'np' :");
    int *p, np, nq, *q;
    scanf("%i", &np);
    printf("Enter the second number which will be stored in 'nq' :");
    scanf("%i", &nq);
    printf("\nCreating a pointer '*p' to np and another pointer 'nq' to nq....\n");

    p=&np;
    q=&nq;
    printf("The value pointed to by *p is :%i(from memory location of np)\nThe value pointed to by *q is :%i(from memory location of nq)\n", *p, *q);

    printf("\nNow swaping your numbers with these pointers....\n");

    p=&nq;
    q=&np;
    printf("\nThe value pointed to by *p is :%i(from memory location of nq)\nThe value pointed to by *q is :%i(from memory location of np)\nSwapping Done right :)\n", *p, *q);

    return 0;
}
