#include <stdio.h>

int main()
{
    printf("Enter the ten elements of your integer array:\n");
    int arr[10], val;

    int c=0;
    for(;c<10; c++)
    {
        printf("Element no. %i:", c+1);
        scanf("%i", &val);
    }

    printf("Now enter the number you want to search for :");
    int search, pos=-1;
    scanf("%i", &search);
    for(c=0; c<10; c++)
    {
        if(arr[c]==search)
            {
                pos=c;
                break;
            }

        }
        if(pos==-1)
        {
            printf("Your number was not found in the array!!");
        }
        else
        {
            printf("Your number was found at index position :%i", pos);
        }





    return 0;
}

