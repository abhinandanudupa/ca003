#include <stdio.h>
char read(int n)
{
    char sup[2];
    if (n%10==1)
        {
			sup[0]='s';
			sup[1]='t';
		}
    else if (n%10==2)
    {
			sup[0]='n';
			sup[1]='d';
		}
    else if (n%10==3)
    {
			sup[0]='r';
			sup[1]='d';
		}
    else
    {
			sup[0]='t';
			sup[1]='h';;
    }
    printf("Enter the grade obtained by the %i%s student: ", n, sup);
    char ch;
    scanf("\r%c", &ch);
    if((ch>=65&&ch<=70)||ch=='S')
        return ch;
    else
        {
			printf("\nYou have entered a wrong grade or you have not entered the grade in capitals!!\n\n");
			return read(n);
        }
}
void count(char ch, int *g_70, int *l_50)
{
    if((ch<='B')||ch=='S')
        ++(*g_70);
    else
        ++(*l_50);
}    

int main()
{
    printf("This program calculates the no. of students scoring above 70 and that of those scoring less than 50 in CCP\n");
    printf("Enter the  total number of students who have taken the exam :\n");
    int num, g_70=0, l_50=0, counter=1;
    scanf("%i", &num);
    while(counter<=num)
    {
        char d=read(counter);
        count(d, &g_70, &l_50);
        counter++;
    }
    printf("The no. of students scoring more than 70 is %d and the no. of students scoring less than 50 is %d\n", g_70, l_50);
    return 0;
}
