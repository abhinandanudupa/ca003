#include <stdio.h>
int toMin(int hrs, int min)
{
	return hrs*60+min;
}
int main()
{
	int hours, minutes;
	printf("\nEnter the time in this format - HH:MM : ");
	scanf("%i : %i", &hours, &minutes);
	int time =toMin(hours, minutes);
	printf("Your time in minutes is: %i.\n\n", time);
	return 0;
}

