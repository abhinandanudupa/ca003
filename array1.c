#include <stdio.h>
int input(int index)
{
	int k;
	printf("Enter integer no. %i : ", index+1);
	scanf("%i", &k);
	return k;
}
			
int main()
{
	printf("Enter the the number of integers you are going to enter: ");
	int len; 
	scanf("%i", &len);
	int list[len];
	if(len>0)
	{
		int count;
		for(count=0;(count<len); count++)
			{
				list[count]=input(count);
			}
	}
	else
	{
		printf("Enter a number greater than 0!!\n");
		return	main();
	}
	
	printf("Now calculating the index of the smallest integer!\n");
	int c, s_index=0;
	for(c=0; c<len; c++)
	{

			if(list[c]<list[s_index])
			{
				s_index=c;
			}
			
		}
	printf("The index position of the smallest number is : %i\n", s_index);
	return 0;

	
}
