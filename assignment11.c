
#include <stdio.h>
#include <string.h>

void input(char k[])
{

    printf("\nEnter the string :");
    gets(k);
}
int chg_case(char g[])
{
    int c=0;
    for(; c<strlen(g); c++)
    {
        if(g[c]>='a' && g[c]<='z')
        {
            g[c]=(char)(g[c]-32);
        }

        else if(g[c]>='A' && g[c]<='Z')
        {
            g[c]=(char)(g[c]+32);
        }
    }
    return 0;
}
void display(char j[])
{
    printf("\nThe string with its case changed is :%s", j);
}


int main()
{
    char s[200];
    input(s);
    if(strlen(s)>0)
    {
        chg_case(s);
        display(s);
    }
    else
    {
        printf("\nEnter a string !!!Try again.....\n");
        main();
    }
    return 0;
}

