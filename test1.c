#include <stdio.h>
#include <math.h>
int main()
{
	printf("\nEnter the x coordinate of the First point: ");
	float x1, x2, y1,y2;
	scanf("%f", &x1);
	printf("Enter the y coordinate of the First point: ");
	scanf("%f", &y1);
	printf("\nEnter the x coordinate of the Second point: ");
	scanf("%f", &x2);
	printf("Enter the y coordinate of the Second point: ");
	scanf("%f", &y2);
	float distance=pow(pow(x2-x1, 2)+pow(y2-y1, 2), 0.5);
	printf("The distance between your two points is: %.4f\n", distance);
	return 0;
}
