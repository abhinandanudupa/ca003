#include <stdio.h>
int Input()
{
	printf("Enter any number: ");
	int num;
	scanf("%i", &num);
	if(num>0)
	{
		return num;
	}
	else
	{
		return Input();
	}	
}
 
int digits(int num)
{
	int n_d=0;
	do
	{
		num/=10;
		n_d++;
	}
	while(num>0);
	return n_d;
}

void display(int n)
{
	printf("The number of digits in your number is : %i", n);
}
int main()
{
	display(digits(Input()));
	return 0;
}
