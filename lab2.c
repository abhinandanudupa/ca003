#include <stdio.h>
#include <math.h>
#define pi 22.0/7.0
float cir_area(float r)
{
    return pi*pow(r,2);
}
int main()
{
    printf("Enter the radius of the circle: ");
    float radius=0, area=0;
    scanf("%f", &radius);
    if (radius>0)
    {
        area=cir_area(radius);
        printf("The area of your circle is: %.4f\n", area);
    }
    else 
    { printf("Invalid Input!!Try again.....");
    }
    return 0;
}


