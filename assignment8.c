#include <stdio.h>

int main()
{
    printf("Enter the number of elements you are going to enter: ");
    int len;
    scanf("%i", &len);
    int c;

    if (len>0)
    {
        int list[len];
        for(c=0; c<len; c++)
        {
            printf("Enter no.%i :", c+1);
            scanf("\r%i",&list[c] );
        }

        float average=0;
        int count;
        for(count=0; count<len; count++)
        {
            average=(average*count+list[count])/(count+1);
        }

        printf("The average of your elements of your array is: %.4f\n", average);

    }
    else
    {
        printf("Enter a number greater than 0!!\nTry again.....\n");
        main();
    }
    return 0;
}
