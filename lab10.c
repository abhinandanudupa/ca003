#include <stdio.h>

int input(int *p, int* q)
{
    printf("Enter the first integer :");
    scanf("\r%i", p);
    printf("Enter the second integer :");
    scanf("\r%i", q);

    return 0;
}
int add(int *r, int *p, int *q)
{
    *r=*p+*q;
    return 0;
}
int sub(int *r, int *p, int *q)
{
    *r=*p-*q;
    return 0;
}
int div(int *r, int *p, int *q)
{
    *r=*p / *q;
    return 0;
}
int mul(int *r, int *p, int *q)
{
    *r=*p * *q;
    return 0;
}
int rem(int *r, int *p, int *q)
{
    *r=*p % *q;
    return 0;
}

int main()
{
    int num1, num2, result,*n1=&num1, *n2=&num2, *r=&result;
    input(n1, n2);
    printf("What do you want to do:\nEnter '1' for addition\nEnter '2' for substraction\nEnter '3' for division\nEnter '4' for Multiplication\nEnter '5' to find the remainder\nEnter 'q' to exit\n");
    char ch='k';

    while(ch!='1'||ch!='2'||ch!='3'||ch!='4'||ch!='5'||ch!='q')
    {
        scanf("\r%c", &ch);
        switch(ch)
        {
            case '1':
                {
                    add(r, n1, n2);
                    printf("The sum of your numbers is :%i\n", result);
                    break;
                }
            case '2':
                {
                    sub(r, n1, n2);
                    printf("The result of your subtraction is :%i\n", result);
                    break;
                }
            case '3':
                {
                    div(r, n1, n2);
                    printf("The quotient of your division is :%i\n", result);
                    break;
                }
            case '4':
                {
                    mul(r, n1, n2);
                    printf("The product of your numbers is :%i\n", result);
                    break;
                }
            case '5':
                {
                    rem(r, n1, n2);
                    printf("The remainder obtained is :%i\n", result);
                    break;
                }
            case 'q':
                {
                    printf("You have exited the program.");
                    break;
                }
            default:
                {
                    printf("You have not entered the correct choice please try again:-\n");
                    continue;
                }
        }
        break;
    }

    return 0;
}
