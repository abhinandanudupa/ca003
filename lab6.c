
#include <stdio.h>

int * input(int arr[], int len)
{
    int c;
    for(c=0; c<len; c++)
    {
        printf("Enter value no.%i: ", c+1);
        scanf("\r%i", &arr[c]);
    }
    return arr;
}

float average(int p[], int len)
{
    int c;
    float avg=0;
    for(c=0; c<len; c++)
    {
        avg=(avg*c + p[c])/(float)(c+1);
    }
    return avg;
}

void display(float k)
{
    printf("\nThe average of your elements is : %f\n", k);
}

int main()
{
    printf("\n\nEnter the no. of elements in your array: ");
    int len;
    scanf("\r%i", &len);
    printf("\n");
    if(len<1)
    {
        printf("\nEnter a value greater than 0\n");
        return main();
    }
    int list[len];
    display(average(input(list,len), len));
    return 0;
}

