#include <stdio.h>

int main()

{
    printf("Enter the number of values you will enter: ");
    int len;
    scanf("%i", &len);
    if(len<0)
    {
        printf("\nYou have entered a number less than zero.Try again.......\n");
        main();
    }
    int list[len];
    int c;
    for(c=0;c<len;c++)
    {
        printf("Enter element no.%i : ", c+1);
        int k;
        scanf("\r%i", &k);
        list[c]=k;
    }
    printf("Enter the value which you want to find the index position of : ");
    int num;
    scanf("\r%i", &num);
    int pos_num=0, pos[len];
    for(c=0; c<len; c++)
    {
        if(list[c]==num)
        {
            pos[pos_num]=c;
            pos_num++;
        }
    }
    if(pos_num>0)
    {
        printf("The value was found at position(s):\n");
        for(c=0; c<pos_num; c++)
        {
            printf("Position: %i\n", pos[c]);
        }
    }
    else
    {
        printf("\nThe value was not found at any position.\n");
    }






    return 0;
}

