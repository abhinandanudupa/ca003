#include <stdio.h>

int main()
{
    FILE *in= fopen("INPUT", "w");
    char data[200];
    printf("Enter any thing you want(max 199 characters) to store in your file:\n");
    fgets(data, 200, stdin);
    fprintf(in, "%s\n", data);
    fclose(in);
    printf("Your data has been saved in to the file.\nNow reading the data in your file.\n");
    in=fopen("INPUT", "r");
    char stored[200];
    fgets(stored, 200, in);
    printf("Data in file is :%s", stored);
    fclose(in);
    return 0;
}
